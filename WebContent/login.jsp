<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Khum Khun Wang Resort</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="css/jcarousel.css" rel="stylesheet" />
<link href="css/flexslider.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link href="skins/default.css" rel="stylesheet" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<jsp:include page="template/header.jsp"></jsp:include>

<div id="wrapper">
	
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="index.jsp"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Login</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	
	
	
	
	
	
	
	<section id="content">
	<div class="container">
	
		<div class="row">
			<div class="col-lg-12">
			 <h3  align="center">ระบบบริหารจัดการการจองห้องพักคุ้มขุนวางรีสอร์ทผ่านระบบมือถือ Android Application</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
					

					<form role="form" action="svlogin" method="post">
                        <div class="row">
                        <div class="form-group col-lg-4"></div>
                        <div class="form-group col-lg-4">
                        
                           <h3>Login เข้าสู่ระบบ</h3>
                    	</div>
	                    <div class="clearfix"></div>
                            <div class="form-group col-lg-4"></div>
                            <div class="form-group col-lg-4">
                                <input id="username" name="username" class="form-control" type="text" placeholder="Username" required="required">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-4"></div>
                            <div class="form-group col-lg-4">
                                <input id="password" name="password" class="form-control" type="password" placeholder="Password" required="required">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-4"></div>
                            <div class="form-group col-lg-4">
                            	
                                <input type="hidden" name="save" value="contact">
                                <button type="submit" class="btn btn-theme">&nbsp;&nbsp;&nbsp;Login&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="col-lg-4"></div>
                        	<div class="col-lg-4">
                           		<label id="errorLabel" style="color:red">${errorMessage}</label></p>
                    		</div>
                        </div>
                    </form>


					</div>
			</div>
		</div>
	</section>
	
	
	
	
	
	
	<jsp:include page="template/footer.jsp"></jsp:include>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/portfolio/jquery.quicksand.js"></script>
<script src="js/portfolio/setting.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>

</body>
</html>