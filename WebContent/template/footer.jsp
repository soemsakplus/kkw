<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Get in touch with us</h5>
					<address>
					<strong>คุ้มขุนวางรีสอร์ท</strong><br>
					<strong>Khum Khun Wang Resort</strong><br>
					 ที่อยู่ 15/2 หมู่ 2 ตำบลบ้านกาด อำเภอแม่วาง จังหวัดเชียงใหม่ 50360</address>
					<p>
						<i class="icon-phone"></i>โทร. 053-928075 <br>
					</p>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
				</div>
			</div>
			<div class="col-lg-3">
			</div>
		</div>
	</div>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
							<span>&copy; Khum Khun Wang Resort 2010 All right reserved. By </span><a href="http://www.everfid.com" target="_blank">Khum Khun Wang Resort. </a><span> Version 0.0.1 Beta</span>
						</p>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<!--li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li-->
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>
