<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp">Khum Khun Wang <span> Resort</span></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="index.jsp">หน้าแรก</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Product<b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="typography.html">รถยนต์</a></li>
                                <li><a href="components.html">รถมอเตอร์ไชค์</a></li>
								<li><a href="pricingbox.html">รถยนต์</a></li>
                            </ul>
                        </li>
                        <li><a href="portfolio.html">ติดต่อเรา</a></li>
                        <li><a href="blog.html">ระบบ</a></li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">ข้อมูลของฉัน  <b class=" icon-angle-down"><span class="badge badge-notify">5</span></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="typography.html">แจ้งเตือน <span class="badge">5</span></a></li>
                                <li><a href="components.html">ข้อมูลของฉัน</a></li>
								<li><a href="pricingbox.html">Log out</a></li>
                            </ul>
                        </li>
						
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
