<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Khum Khun Wang Resort</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="css/jcarousel.css" rel="stylesheet" />
<link href="css/flexslider.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link href="skins/default.css" rel="stylesheet" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<jsp:include page="template/header.jsp"></jsp:include>

<div id="wrapper">
	
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="index.jsp"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Home</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<article>
						<div class="post-image">
							<div class="post-heading">
								<h3><a href="#">ระบบบริหารจัดการการจองห้องพักคุ้มขุนวางรีสอร์ทผ่านระบบมือถือ Android Application</a></h3>
							</div>
							<img src="img/kkw.jpg" alt=""/>
						</div>
						<p>
						“คุ้มขุนวางรีสอร์ท” บูติครีสอร์ทระดับ 3 ดาว ตกแต่งด้วยศิลปหัตถกรรมอันวิจิตรงดงามที่ทรงคุณค่าด้วยวัฒนธรรม ตั้งอยู่เลขที่ 15/2 หมู่ 2 ตำบลบ้านกาด อำเภอแม่วาง จังหวัดเชียงใหม่ (ตรงข้ามสถานีตำรวจอำเภอแม่วาง) ระยะทางประมาณ 32 กิโลเมตรจากสนามบินนานาชาติ จังหวัดเชียงใหม่ เพียง 20 นาทีจากตัวเมืองเชียงใหม่ 
“คุ้มขุนวางรีสอร์ท” บูติครีสอร์ท 9 ห้อง ที่ตกแต่งด้วยศิลปหัตถกรรมสไตล์ล้านนาและเครื่องอำนวยความสะดวกที่แตกต่าง แวดล้อมด้วยธรรมชาติ และความสะดวกสบายครบครัน พร้อมการต้อนรับที่อบอุ่น บริการดุจญาติมิตรจากเรา และความเป็นมิตรจริงใจจากคนในท้องถิ่นที่ท่านจะต้องประทับใจไม่รู้ลืม
“คุ้มขุนวางรีสอร์ท” บริการท่านด้วยห้องพัก 9 ห้อง ตกแต่งสไตล์ล้านนาพร้อมระเบียงส่วนตัวทุกห้อง
						</p>
						
				</article>
				
			</div>
			
		</div>
	</div>
	</section>
	
	
	<jsp:include page="template/footer.jsp"></jsp:include>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/portfolio/jquery.quicksand.js"></script>
<script src="js/portfolio/setting.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
</body>
</html>