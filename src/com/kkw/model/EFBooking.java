package com.kkw.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.everfid.data.EFEntity;

@Table
@Entity
@Cacheable
public class EFBooking extends EFEntity {

	private static final long serialVersionUID = -3283651507054712959L;
	@Column
	String roomname;
	
	@Column
	Date checkin;
	
	@Column
	String night;
	
	@Column
	String room;
	
	@Column
	String extra;
	
	@Column
	String bookingName;
	
	@Column
	String tel;
	
	@Column
	String status;
	
	@Column
	String total;
	
	@Column
	String email;
	
	@OneToMany(mappedBy="booking")
	List<EFRoomBooking> roomBookings;

	@ManyToOne
	@JoinColumn(name="rooms")
	EFRoom rooms;
	
	
	
	public List<EFRoomBooking> getRoomBookings() {
		return roomBookings;
	}

	public void setRoomBookings(List<EFRoomBooking> roomBookings) {
		this.roomBookings = roomBookings;
	}

	public EFRoom getRooms() {
		return rooms;
	}

	public void setRooms(EFRoom rooms) {
		this.rooms = rooms;
	}

	public String getRoomname() {
		return roomname;
	}

	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}

	public Date getCheckin() {
		return checkin;
	}

	public void setCheckin(Date checkin) {
		this.checkin = checkin;
	}

	public String getNight() {
		return night;
	}

	public void setNight(String night) {
		this.night = night;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getBookingName() {
		return bookingName;
	}

	public void setBookingName(String bookingName) {
		this.bookingName = bookingName;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
