package com.kkw.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.everfid.data.EFEntity;

@Table
@Entity
@Cacheable
public class EFRoom extends EFEntity {

	private static final long serialVersionUID = 7021036439367314480L;
	
	@OneToMany(mappedBy="rooms")
	List<EFBooking> bookings;
	
	@Column
	String type;
	
	@OneToMany(mappedBy="room")
	List<EFRoomBooking> roomBookings;
	
	

	public List<EFRoomBooking> getRoomBookings() {
		return roomBookings;
	}

	public void setRoomBookings(List<EFRoomBooking> roomBookings) {
		this.roomBookings = roomBookings;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<EFBooking> getBookings() {
		return bookings;
	}

	public void setBookings(List<EFBooking> bookings) {
		this.bookings = bookings;
	}
}
