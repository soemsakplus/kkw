package com.kkw.model;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.everfid.data.EFEntity;

@Table
@Entity
@Cacheable
public class EFRoomBooking extends EFEntity {

	private static final long serialVersionUID = -3619067992724765545L;

	@Column
	Date bookingDate;
	
	@ManyToOne
	@JoinColumn(name="room")
	EFRoom room;
	
	@ManyToOne
	@JoinColumn(name="booking")
	EFBooking booking;

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public EFRoom getRoom() {
		return room;
	}

	public void setRoom(EFRoom room) {
		this.room = room;
	}

	public EFBooking getBooking() {
		return booking;
	}

	public void setBooking(EFBooking booking) {
		this.booking = booking;
	}
	
	
}
