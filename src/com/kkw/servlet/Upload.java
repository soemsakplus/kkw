package com.kkw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.everfid.data.EFDao;
import com.kkw.model.EFBooking;

/**
 * Servlet implementation class Upload
 */
@WebServlet("/Upload")
public class Upload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Upload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		request.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		
		String name = request.getParameter("n");
		String tel = request.getParameter("t");
		String roomname = request.getParameter("rn");
		String ds = request.getParameter("d");
		String ms = request.getParameter("m");
		String ys = request.getParameter("y");
		String rooms = request.getParameter("r");
		String night = request.getParameter("nt");
		String extra = request.getParameter("ex");
		String total = request.getParameter("tt");
		String email = request.getParameter("e");
		
		
		EFBooking booking = new EFBooking();
		
		booking.setBookingName(name);
		booking.setTel(tel);
		booking.setRoomname(roomname);
		booking.setRoom(rooms);
		booking.setNight(night);
		booking.setExtra(extra);
		booking.setTotal(total);
		booking.setEmail(email);
		
		booking.setStatus("รอการยืนยัน");
		
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(ys), Integer.parseInt(ms) - 1, Integer.parseInt(ds), 0, 0); 
		booking.setCheckin(c.getTime());
		
		new EFDao<EFBooking>(EFBooking.class).addItem(booking);
		
		out.print(booking.getId());
		
	}

}
