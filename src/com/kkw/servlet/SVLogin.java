package com.kkw.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.everfid.context.EFWebContext;

@WebServlet(name = "svlogin", urlPatterns = { "/svlogin" })
public class SVLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SVLogin() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter(); 
        HttpSession session = request.getSession();
        
        Cookie cookie = null;
  	  	Cookie[] cookies = null;
        cookies = request.getCookies();
        
		String username = request.getParameter("username");  
        String password = request.getParameter("password");
        
        session.removeAttribute(EFWebContext.getUserSessionName());
        for (Cookie c : cookies) {
        	c.setMaxAge(0);
            response.addCookie(c);
		}

        // start login again
        EntityManager entityManager = EFWebContext.createEntityManager();
        try {
			if(username.equals("admin") && password.equals("admin")){
				
				session.setAttribute(EFWebContext.getUserSessionName(), "admin");
		        session.setMaxInactiveInterval(60*60*24*5);
		        
		        entityManager.close();
		        response.sendRedirect("booking.jsp");
		        return;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        entityManager.close();

        request.setAttribute("errorMessage","Login failed. Please try again. (รหัสของท่านผิด)");
        response.sendRedirect("login.jsp");
	}

}
