package com.kkw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.everfid.context.EFWebContext;
import com.everfid.data.EFDao;

/**
 * Servlet implementation class SVLogout
 */
@WebServlet(name = "svlogout", urlPatterns = { "/svlogout" })
public class SVLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SVLogout() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		doPost(req,resp);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
        PrintWriter out=response.getWriter(); 
        HttpSession session = request.getSession();
        
        Cookie cookie = null;
  	  	Cookie[] cookies = null;
        cookies = request.getCookies();

        session.removeAttribute(EFWebContext.getUserSessionName());
        session.invalidate();
        for (Cookie c : cookies) {
        	c.setMaxAge(0);
            response.addCookie(c);
		}
        
        response.sendRedirect("login.jsp");
	}

}
