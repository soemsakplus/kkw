package com.kkw.zk;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;

import com.everfid.data.EFDao;
import com.everfid.ui.UIComboAdapter;
import com.everfid.ui.UIComboBox;
import com.kkw.model.EFBooking;
import com.kkw.model.EFRoom;
import com.kkw.model.EFRoomBooking;

public class ZKAddBooking extends GenericForwardComposer<Component> {

	private static final long serialVersionUID = 7056414271538181297L;
	
	Datebox beginDatebox,endDatebox;
	
	UIComboBox customerNameCombobox,
	telCombobox,
	emailCombobox,
	roomTypeCombobox,
	roomCombobox,
	statusCombobox;
	
	Decimalbox roomDoublebox,
	extraDoublebox,
	totalDoublebox;
	
	Button addButton;
	
	List<EFRoom> roomsList;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		//roomCombobox.initComboboxFromEntity(EFRoom.class);
		
		roomTypeCombobox.addEventListener("onChange", new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				onClick$calButton(event);
			}
		});
		
		beginDatebox.addEventListener("onChange", new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				onClick$calButton(event);
			}
		});
		
		endDatebox.addEventListener("onChange", new EventListener<Event>() {

			@Override
			public void onEvent(Event event) throws Exception {
				onClick$calButton(event);
			}
		});
	}
	
	public void onClick$calButton(Event event){
		if(beginDatebox.getValue()!=null && endDatebox.getValue()!=null && !roomTypeCombobox.getValue().equals("")){
			
			roomsList = new EFDao<EFRoom>(EFRoom.class).getItemsByColumnName("type", roomTypeCombobox.getValue());
			List<EFRoomBooking> rb = new EFDao<EFRoomBooking>(EFRoomBooking.class).getItems();
			for (EFRoomBooking efRoomBooking : rb) {
				Date bDate = efRoomBooking.getBookingDate();
				if(bDate.after(beginDatebox.getValue()) && bDate.before(endDatebox.getValue())){
					EFRoom rr = efRoomBooking.getRoom();
					for (EFRoom rx : roomsList) {
						if(rx.getId() == rr.getId()){
							roomsList.remove(rx);
							break;
						}
					}
				}
			}
			
			roomCombobox.setAdapter(new UIComboAdapter() {
				
				@Override
				public List<?> getComboItemList(String filterString) {
					return roomsList;
				}
			});
			roomCombobox.reloadData("");
			
		}else{

			roomCombobox.setAdapter(new UIComboAdapter() {
				
				@Override
				public List<?> getComboItemList(String filterString) {
					return null;
				}
			});
			roomCombobox.reloadData("");
		}
		
	}
	
	public void onClick$addButton(Event event){
		Date d = new Date();
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);
		
		if(beginDatebox.getValue() ==null){
			beginDatebox.setErrorMessage("วันที่ผิด");
			return;
		}
		Date d2 = beginDatebox.getValue();
		d2.setHours(1);
		if(d2.before(d)){
			beginDatebox.setErrorMessage("วันที่ผิด");
			return;
		}

		if(endDatebox.getValue() ==null){
			endDatebox.setErrorMessage("วันที่ผิด");
			return;
		}
		
		if(endDatebox.getValue().before(beginDatebox.getValue())){
			endDatebox.setErrorMessage("วันที่ผิด");
			return;
		}
		
		String name = customerNameCombobox.getValue();

		if(name==null || name.equals("")){
			customerNameCombobox.setErrorMessage("ข้อมูลผิด");
			return;
		}
			
		String tel = telCombobox.getValue();

		if(tel==null || tel.equals("")){
			telCombobox.setErrorMessage("ข้อมูลผิด");
			return;
		}
		
		String roomname = roomTypeCombobox.getValue();

		if(roomname==null || roomname.equals("")){
			roomTypeCombobox.setErrorMessage("ข้อมูลผิด");
			return;
		}

		long startTime = beginDatebox.getValue().getTime();
		long endTime = endDatebox.getValue().getTime();
		long diffTime = endTime - startTime + 1;
		long diffDays = diffTime / (1000 * 60 * 60 * 24);

		String ds = new SimpleDateFormat("dd").format(beginDatebox.getValue()); ;
		String ms = new SimpleDateFormat("MM").format(beginDatebox.getValue()); ;
		String ys = new SimpleDateFormat("yyyy").format(beginDatebox.getValue()); 
		
		if(roomDoublebox.getValue() == null || ( roomDoublebox.getValue().intValue()<=0 || roomDoublebox.getValue().intValue()>3)){
			roomDoublebox.setErrorMessage("ข้อมูลผิด");
			return;
		}
		if(extraDoublebox.getValue() == null || ( extraDoublebox.getValue().intValue()<0 || extraDoublebox.getValue().intValue()>3)){
			extraDoublebox.setErrorMessage("ข้อมูลผิด");
			return;
		}
//		if(roomCombobox.getValue() == null || roomCombobox.getValue().equals("")){
//			extraDoublebox.setErrorMessage("ข้อมูลผิด");
//			return;
//		}
		
		EFRoom r = new EFDao<EFRoom>(EFRoom.class).getSingleItemByColumnName("uniqueName", roomCombobox.getValue());
		
		String rooms = roomDoublebox.getValue() + "";
		diffDays = diffDays + 1;
		String night = diffDays + "";
		String extra = extraDoublebox.getValue() + "";
		String total = "0";
		String email = emailCombobox.getValue();
		
		EFBooking booking = new EFBooking();
		
		booking.setBookingName(name);
		booking.setTel(tel);
		booking.setRoomname(roomname);
		booking.setRoom(rooms);
		booking.setNight(night);
		booking.setExtra(extra);
		booking.setTotal(total);
		booking.setEmail(email);
		
		if(statusCombobox.getValue()==null || statusCombobox.getValue().equals("")){
			booking.setStatus("ระบุสถานะการจองแล้ว");
		}else{
			booking.setStatus(statusCombobox.getValue());
		}
		booking.setRooms(r);
		
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(ys), Integer.parseInt(ms) - 1, Integer.parseInt(ds), 0, 0); 
		booking.setCheckin(c.getTime());
		
		
		new EFDao<EFBooking>(EFBooking.class).addItem(booking);
		
		for(int i = 0;i < diffDays;i++){
			EFRoomBooking rb = new EFRoomBooking();
			rb.setBooking(booking);
			Calendar ca = Calendar.getInstance();
			ca.set(Integer.parseInt(ys), Integer.parseInt(ms) - 1, Integer.parseInt(ds)+i, 0, 0); 
			rb.setBookingDate(ca.getTime());
			rb.setRoom(r);
			new EFDao<EFRoomBooking>(EFRoomBooking.class).addItem(rb);
		}
		

		try {
			final String username = "kkw.resort@gmail.com";
			final String password = "a012345678";
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
	 
			if(booking.getRooms()!=null){
				try {
					 
					MimeMessage message = new MimeMessage(session);
					message.setFrom(new InternetAddress(username));
					message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(booking.getEmail()));
					message.setSubject("สถานะการจองห้องพักของคุณ", "UTF-8");
					
					
					message.setText("ถึงคุณ "+booking.getBookingName()+","
						+ "\n\n ตอนนี้สถานะการจองของคุณคือ " + booking.getStatus() + " คุณได้จองห้องเลขที่ " + booking.getRooms().getUniqueName() + " ทั้งนี้คุณยังสามารถตรวจสอบสถานะการจองห้องพักได้ใน Mobile application ได้อีกด้วย", "UTF-8");
		 
					Transport.send(message);
		 
					System.out.println("Sent email done");
		 
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
			}
		} catch (Exception e) {
		}
		
		Executions.sendRedirect("/booking.jsp");
	}

}
