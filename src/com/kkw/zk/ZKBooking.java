package com.kkw.zk;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Label;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Vbox;

import com.everfid.data.EFDao;
import com.everfid.ui.IButtonClickedListener;
import com.everfid.ui.UIEditableLabel;
import com.everfid.ui.UIGrid;
import com.everfid.ui.UIGridAdapter;
import com.kkw.model.EFBooking;
import com.kkw.model.EFRoom;

public class ZKBooking extends GenericForwardComposer<Component> {

	private static final long serialVersionUID = 7658092268974076093L;
	
	UIGrid grid;
	Paging paginge;
	GridAdapter adapter;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);

		EFDao<EFRoom> dao = new EFDao<EFRoom>(EFRoom.class);
		EFRoom r = dao.getOrCreateItemByUniqueName("A101");
		r.setType("Superior room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A102");
		r.setType("Superior room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A103");
		r.setType("Superior room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A104");
		r.setType("Family room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A105");
		r.setType("Family room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A106");
		r.setType("Family room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A107");
		r.setType("Deluxe room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A108");
		r.setType("Deluxe room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A109");
		r.setType("Deluxe room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A110");
		r.setType("Executive room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A111");
		r.setType("Executive room");
		dao.updateItem(r);
		r = dao.getOrCreateItemByUniqueName("A112");
		r.setType("Executive room");
		dao.updateItem(r);
		
		///////////////////////////////////////////////////
		
		adapter = new GridAdapter();
		grid.setAdapter(adapter);
		grid.onEvent(null);
	}

	public class GridAdapter extends UIGridAdapter {

		@Override
		public Rows getRowsFromPage(int pageIndex, int firstResultIndex,
				int pagesize, Object filterObject, String sortingString) {

			List<EFBooking> list = (List<EFBooking>) new EFDao(EFBooking.class).setOrderby("Order by createdDate DESC").getItems();
			Rows rows = new Rows();
			if(list==null){
				return rows;
			}
		    for (EFBooking object : list) {
				Row row = new Row();
				row.setParent(rows);
				////////////////////////
				String editDateString = new SimpleDateFormat("yyyy-MM-dd").format(object.getEditedDate()); 
				////////////////////////
			    Vbox v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    Label lb1 = new Label(editDateString);
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
				editDateString = new SimpleDateFormat("yyyy-MM-dd").format(object.getCheckin()); 
				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(editDateString);
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
			    String n = object.getNight();
			    long l = Long.parseLong(n);
			    Date xDate = new Date(object.getCheckin().getTime() + (1000*60*60*24*l));
				editDateString = new SimpleDateFormat("yyyy-MM-dd").format(xDate); 
				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(editDateString);
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);
			    
				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(object.getRoomname());
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(object.getNight());
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(object.getExtra());
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);


				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(object.getBookingName());
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(object.getTel());
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
			    v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    lb1 = new Label(object.getEmail());
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
			    Vbox h1 = new Vbox();
			    h1.setHflex("1");
			    h1.setParent(row);
			    UIEditableLabel eb1 = new UIEditableLabel(object.getStatus());
			    eb1.getLabel().setStyle("font-size: 10px;");
			    eb1.setAttribute("object", object);
			    eb1.setHflex("1");
			    eb1.setParent(h1);
			    eb1.setOnButtonClickedListener(new IButtonClickedListener() {
					@Override
					public void onIButtonClicked(Object sender, Event event) {
						UIEditableLabel editableLabel = (UIEditableLabel)sender;
						EFBooking product = (EFBooking) editableLabel.getAttribute("object");
						EFDao<EFBooking> dao = new EFDao<EFBooking>(EFBooking.class);
						product = (EFBooking) dao.getItemById(product.getId());
						product.setStatus(editableLabel.getLabel().getValue());
						dao.updateItem(product);
						editableLabel.setAttribute("object", product);
						
						/////email
						final String username = "kkw.resort@gmail.com";
						final String password = "a012345678";
						Properties props = new Properties();
						props.put("mail.smtp.auth", "true");
						props.put("mail.smtp.starttls.enable", "true");
						props.put("mail.smtp.host", "smtp.gmail.com");
						props.put("mail.smtp.port", "587");
						Session session = Session.getInstance(props,
						  new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(username, password);
							}
						  });
				 
						try {
				 
							MimeMessage message = new MimeMessage(session);
							message.setFrom(new InternetAddress(username));
							message.setRecipients(Message.RecipientType.TO,
								InternetAddress.parse(product.getEmail()));
							message.setSubject("สถานะการจองห้องพักของคุณ", "UTF-8");
							message.setText("ถึงคุณ "+product.getBookingName()+","
								+ "\n\n ตอนนี้สถานะการจองของคุณคือ " + product.getStatus() + " ทั้งนี้คุณยังสามารถตรวจสอบสถานะการจองห้องพักได้ใน Mobile application ได้อีกด้วย", "UTF-8");
				 
							Transport.send(message);
				 
							System.out.println("Sent email done");
				 
						} catch (MessagingException e) {
							throw new RuntimeException(e);
						}
					}
				});
				////////////////////////
			    String showRooms = "";
			    if(object.getRooms()!=null){
			    	showRooms =  object.getRooms().getUniqueName();
			    }
			    
			    addLabel(row, object, showRooms);
			    /*
			    UIEditableComboBox c = addEditableCombobox(row, object, showRooms, EFRoom.class, new UIComboAdapter() {
					
					@Override
					public List<?> getComboItemList(String filterString) {
						
						EFDao<EFRoom> dao = new EFDao<EFRoom>(EFRoom.class);
						
						return dao.getItemsByColumnName("type", filterString);
					}
				}, new IButtonClickedListener() {
					
					@Override
					public void onIButtonClicked(Object sender, Event event) {

						UIEditableComboBox editableCombo = (UIEditableComboBox)sender;
						EFBooking product = (EFBooking) editableCombo.getAttribute("object");
						
						EFDao dao = new EFDao(EFBooking.class);
						product = (EFBooking) dao.getItemById(product.getId());
						try {
							product.setRooms((EFRoom)editableCombo.getComboBox().getSelectedItem().getAttribute("object"));
							dao.updateItem(product);
							editableCombo.setAttribute("object", product);
							/////email
							final String username = "kkw.resort@gmail.com";
							final String password = "a012345678";
							Properties props = new Properties();
							props.put("mail.smtp.auth", "true");
							props.put("mail.smtp.starttls.enable", "true");
							props.put("mail.smtp.host", "smtp.gmail.com");
							props.put("mail.smtp.port", "587");
							Session session = Session.getInstance(props,
							  new javax.mail.Authenticator() {
								protected PasswordAuthentication getPasswordAuthentication() {
									return new PasswordAuthentication(username, password);
								}
							  });
					 
							if(product.getRooms()!=null){
								try {
									 
									MimeMessage message = new MimeMessage(session);
									message.setFrom(new InternetAddress(username));
									message.setRecipients(Message.RecipientType.TO,
										InternetAddress.parse(product.getEmail()));
									message.setSubject("สถานะการจองห้องพักของคุณ", "UTF-8");
									
									
									message.setText("ถึงคุณ "+product.getBookingName()+","
										+ "\n\n ตอนนี้สถานะการจองของคุณคือ " + product.getStatus() + " คุณได้จองห้องเลขที่ " + product.getRooms().getUniqueName() + " ทั้งนี้คุณยังสามารถตรวจสอบสถานะการจองห้องพักได้ใน Mobile application ได้อีกด้วย", "UTF-8");
						 
									Transport.send(message);
						 
									System.out.println("Sent email done");
						 
								} catch (MessagingException e) {
									throw new RuntimeException(e);
								}
							}
						} catch (Exception e) {
							String showRooms = "";
						    if(object.getRooms()!=null){
						    	showRooms =  object.getRooms().getUniqueName();
								editableCombo.getLabel().setValue(showRooms);
						    }else{
						    	editableCombo.getAddButton().setVisible(true);
						    }
						}
						
						
					}
				});
			    
			    c.getComboBox().reloadData(object.getRoomname());
			    */

				////////////////////////
			    addButton(row, object, "แก้ไข", "btn-xs btn-success", new EventListener<Event>() {
					@Override
					public void onEvent(Event arg0) throws Exception {
						Button button = (Button) arg0.getTarget();
						EFBooking booking = (EFBooking) button.getAttribute("object");
						Executions.getCurrent().sendRedirect("editbooking.jsp?id="+booking.getId());
					}
				});
		    }
			return rows;
		}

		@Override
		public int getNumberOfRows(Object filterObject) {
			List<EFBooking> bookings = (List<EFBooking>) new EFDao<EFBooking>(EFBooking.class).getItems();
			return bookings.size();
		}

		@Override
		public Columns getColumns() {
			return null;
		}

		@Override
		public int getPageSize() {
			return 40;
		}
		
	}
	
}
