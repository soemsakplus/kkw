package com.kkw.zk;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Vbox;

import com.everfid.data.EFDao;
import com.everfid.ui.UIGrid;
import com.everfid.ui.UIGridAdapter;
import com.kkw.model.EFRoomBooking;

public class ZKSearchBooking extends GenericForwardComposer<Component> {

	private static final long serialVersionUID = 7056414271538181297L;
	
	Datebox beginDatebox,endDatebox;
	
	Button addButton;
	

	UIGrid grid;
	Paging paginge;
	GridAdapter adapter;
	
	List<EFRoomBooking> roomsList;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
	}
	
	public void onClick$addButton(Event event){
		if(beginDatebox.getValue()!=null && endDatebox.getValue()!=null){
			roomsList = new ArrayList<EFRoomBooking>();
			List<EFRoomBooking> rb = new EFDao<EFRoomBooking>(EFRoomBooking.class).getItems();
			for (EFRoomBooking efRoomBooking : rb) {
				Date bDate = efRoomBooking.getBookingDate();

				Calendar c = Calendar.getInstance(); 
				c.setTime(endDatebox.getValue()); 
				c.add(Calendar.DATE, 1);
				
				if((bDate.after(beginDatebox.getValue()) && bDate.before(c.getTime()))){
					roomsList.add(efRoomBooking);
				}
			}
			adapter = new GridAdapter();
			adapter.setList(roomsList);
			grid.setAdapter(adapter);
			grid.onEvent(null);
		}
	}
	

	public class GridAdapter extends UIGridAdapter<EFRoomBooking> {

		@Override
		public Rows getRowsFromPage(int pageIndex, int firstResultIndex,
				int pagesize, Object filterObject, String sortingString) {

			List<EFRoomBooking> list = this.list;
			Rows rows = new Rows();
			if(list==null){
				return rows;
			}
		    for (EFRoomBooking object : list) {
				Row row = new Row();
				row.setParent(rows);
				////////////////////////
				String editDateString = new SimpleDateFormat("yyyy-MM-dd").format(object.getBookingDate()); 
				////////////////////////
			    Vbox v1 = new Vbox();
			    v1.setHflex("1");
			    v1.setParent(row);
			    Label lb1 = new Label(editDateString);
			    lb1.setStyle("font-size: 10px;");
			    lb1.setAttribute("object", object);
			    lb1.setHflex("1");
			    lb1.setParent(v1);

				////////////////////////
			    if(object.getRoom()!=null){
				    addLabel(row, object, object.getRoom().getUniqueName());
			    }else{
				    addLabel(row, object, "");
			    }
			    
			    addLabel(row, object, object.getBooking().getBookingName());
			    addLabel(row, object, object.getBooking().getTel());
			    addLabel(row, object, object.getBooking().getEmail());
			    addLabel(row, object, object.getBooking().getExtra());
			    addLabel(row, object, object.getBooking().getStatus());
				
		    }
			return rows;
		}

		@Override
		public int getNumberOfRows(Object filterObject) {
			List<EFRoomBooking> bookings = (List<EFRoomBooking>) this.list;
			return bookings.size();
		}

		@Override
		public Columns getColumns() {
			return null;
		}

		@Override
		public int getPageSize() {
			return 100;
		}
		
	}

}
